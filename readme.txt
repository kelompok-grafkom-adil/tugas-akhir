Kelompok IGA
Anggota:
- Muhammad Imbang Murtito (1606889502)
- Muhammad Aulia Adil Murtito (1906398591)
- Gita Permatasari Sujatmiko (1906400053)

*Petunjuk Menjalankan Program*
1. Buka file pada visual studio code
2. Download ekstensi live server
3. Jalankan file dengan live server dari visual studio code
4. Jika canvas terlihat gelap, maka refresh ulang terus-menerus 
hingga objek terlihat. 

*Asumsi ketergantungan Robotic Arm*
Root: Fondasi kotak bawah pada objek.
Arm: Keseluruhan batang dari objek
Elbow: Batang atas dari objek
Wrist: Bagian kotak di atas pada objek
Fingers: Jari-jari objek yang berada pada wrist

Animasi: Semua bagian melakukan rotasi kecuali root.

Data struktur: tree
Root: Arm

            Arm
            /
        Elbow
        /
    Wrist
    /
Finger
