// MultiJointModel_segment.js (c) 2012 matsuda
// Edited by Kelompok IGA (Imbang, Gita, Adil)


// Vertex shader program

//Global variable for texture configuration
var texcoordBuffer = null;
var texcoordLocation = null;

var VSHADER_SOURCE =
  ` attribute vec2 a_texcoord;
    attribute vec4 a_Position;
    attribute vec4 a_Normal;

    uniform vec3 u_LightWorldPosition;
    uniform vec3 u_ViewWorldPosition;
    uniform vec3 u_LightDirection;

    uniform mat4 u_MvpMatrix;
    uniform mat4 u_NormalMatrix;

    uniform float u_Shininess;
    uniform float u_LightingMode;
    uniform float u_InnerLimit;          // in dot space
    uniform float u_OuterLimit;          // in dot space

    varying vec2 v_texcoord;
    
    varying vec4 v_Color;
    
    void main() {
        gl_Position = u_MvpMatrix * a_Position;

        // White color as a neutral color for lighting
        vec4 color = vec4(1, 1, 1, 1);

        // Orient normal
        vec3 normal = normalize((u_NormalMatrix * a_Normal).xyz);

        // Directional lighting
        float directionalLight = max(dot(normal, normalize(u_LightWorldPosition)), 0.0);
        vec4 directional_Color = vec4(color.rgb * directionalLight + vec3(0.2), color.a);
        
        // Point lighting
        // compute the world position of the surface
        vec3 surfaceWorldPosition = (a_Position).xyz;
        
        // compute the vector of the surface to the light
        vec3 surfaceToLightDirection = normalize(u_LightWorldPosition - surfaceWorldPosition);

        // compute the vector of the surface to the view/camera
        vec3 surfaceToViewDirection = normalize(u_ViewWorldPosition - surfaceWorldPosition);

        vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);
        
        float pointLight = max(dot(normal, surfaceToLightDirection), 0.0);
        float pointLightSpecular = 0.0;
        if (pointLight > 0.0) {
            pointLightSpecular = pow(dot(normal, halfVector), u_Shininess);
        }

        vec4 point_Color = vec4(color.rgb * pointLight + vec3(0.1) + pointLightSpecular, color.a);

        // Spot lighting
        float dotFromDirection = dot(surfaceToLightDirection, -u_LightDirection);
        float inLight = smoothstep(u_OuterLimit, u_InnerLimit, dotFromDirection);
        float spotLight = inLight * dot(normal, surfaceToLightDirection);
        float spotLightSpecular = inLight * pow(dot(normal, halfVector), u_Shininess);
        vec4 spot_Color = vec4((color.rgb * spotLight) + vec3(0.1) + spotLightSpecular, color.a);

        v_Color = directional_Color;
        if (u_LightingMode == 1.0) {
          v_Color = point_Color;
        }
        if (u_LightingMode == 2.0) {
          v_Color = spot_Color;
        }
        
        // Texture
        v_texcoord = a_texcoord;
    }
`;

// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'varying vec2 v_texcoord;\n' +
  'uniform sampler2D thetexture;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color * texture2D(thetexture, vec2(v_texcoord.s, v_texcoord.t));\n' +
  '}\n';

// Uniform variable
var a_texcoord;
var u_LightWorldPosition;
var u_Shininess;
var u_ViewWorldPosition;
var u_MvpMatrix;
var u_NormalMatrix;
var u_LightingMode;
var u_LightDirection;
var u_InnerLimit;
var u_OuterLimit;

function main() {

  // Retrieve <canvas> element
  var canvas = document.getElementById('webgl');

  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }

  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }

  // Set the vertex information
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log('Failed to set the vertex information');
    return;
  }

  // Set the clear color and enable the depth test
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);

  // Get the storage locations of attribute and uniform variables
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  a_texcoord = gl.getAttribLocation(gl.program, 'a_texcoord');
  u_MvpMatrix = gl.getUniformLocation(gl.program, 'u_MvpMatrix');
  u_NormalMatrix = gl.getUniformLocation(gl.program, 'u_NormalMatrix');
  u_LightWorldPosition = gl.getUniformLocation(gl.program, 'u_LightWorldPosition');
  u_ViewWorldPosition = gl.getUniformLocation(gl.program, 'u_ViewWorldPosition');
  u_Shininess = gl.getUniformLocation(gl.program, 'u_Shininess');
  u_LightingMode = gl.getUniformLocation(gl.program, 'u_LightingMode');
  u_LightDirection = gl.getUniformLocation(gl.program, 'u_LightDirection');
  u_InnerLimit = gl.getUniformLocation(gl.program, 'u_InnerLimit');
  u_OuterLimit = gl.getUniformLocation(gl.program, 'u_OuterLimit');
  if (a_Position < 0 || !u_MvpMatrix || !u_NormalMatrix || !u_LightWorldPosition || !u_Shininess || !u_LightingMode) {
    console.log('Failed to get the storage location of attribute or uniform variable');
    return;
  }

  initTexture(gl);

  // Calculate the view projection matrix
  var viewProjMatrix = new Matrix4();
  viewProjMatrix.setPerspective(50.0, canvas.width / canvas.height, 1.0, 100.0);

  // For camera lookat position
  var cameraPosition = {
    x: 30, y: 10, z: 60
  }

  // For target lookat position
  var targetPosition = {
    x: 15, y: -12, z: 12,
  }

  // Multiply view projection matrix with matrix lookat 
  viewProjMatrix.lookAt(
    cameraPosition.x, cameraPosition.y, cameraPosition.z,
    targetPosition.x, targetPosition.y, targetPosition.z,
    0.0, 1.0, 0.0
  );
  
  // Set default lighting mode
  lighting_mode = 0;  // Directional lighting
  gl.uniform1f(u_LightingMode, lighting_mode);

  // Set light world position
  lightWorldPosition = [0, 20, 30];
  gl.uniform3fv(u_LightWorldPosition, lightWorldPosition); 

  // Set shininess
  var shininess = 30;
  gl.uniform1f(u_Shininess, shininess);

  function degToRad(d) {
    return d * Math.PI / 180;
  }

  // Set spot light limit
  var innerLimit = degToRad(6);
  var outerLimit = degToRad(6);
  gl.uniform1f(u_InnerLimit, Math.cos(innerLimit));
  gl.uniform1f(u_OuterLimit, Math.cos(outerLimit));

  // Set light direction
  var lmat = new Matrix4()
  lmat = lmat.lookAt(
                    lightWorldPosition[0], lightWorldPosition[1], lightWorldPosition[2], 
                    0, 20, 0,
                    0.0, 1.0, 0.0
                  );

  // get the zAxis from the matrix
  // negate it because lookAt looks down the -Z axis
  var lightDirection = [-lmat.elements[8], -lmat.elements[9],-lmat.elements[10]];
  gl.uniform3fv(u_LightDirection, lightDirection);


  // Set view world position
  var viewWorldPosition = [cameraPosition.x, cameraPosition.y, cameraPosition.z];
  gl.uniform3fv(u_ViewWorldPosition, viewWorldPosition); 

  // Register the event handler to be called on key press
  document.onkeydown = function (ev) {
    keydown(ev, gl, n, viewProjMatrix, a_Position, cameraPosition, targetPosition, lighting_mode);
  };

  // To initialize animation
  var animationModeButton = document.getElementsByClassName("animation-mode")[0];
  animationModeButton.onclick = function (ev) {
    isInAnimationMode = !isInAnimationMode;
    animationMode(gl, n, viewProjMatrix, a_Position, cameraPosition, targetPosition)
  };

  // Set up polygon mode
  polygon_mode = gl.TRIANGLES;

  // For easy access a variable
  structDraw.gl = gl;
  structDraw.n = n;
  structDraw.viewProjMatrix = viewProjMatrix;
  structDraw.a_Position = a_Position;

  // Button for demo mode
  var carrierArmInteractiveModeButton = document.getElementById("carrier-demo");
  var laserArmInteractiveModeButton = document.getElementById("laser-demo");
  var hammerArmInteractiveModeButton = document.getElementById("hammer-demo");

  //Event handler for demo mode
  carrierArmInteractiveModeButton.onclick = function () {
    interactiveFlag = CARRIER_ARM_DEMO_CODE;
  }

  laserArmInteractiveModeButton.onclick = function () {
    interactiveFlag = LASER_ARM_DEMO_CODE;
  }

  hammerArmInteractiveModeButton.onclick = function () {
    interactiveFlag = HAMMER_ARM_DEMO_CODE;
  }

  requestAnimationFrame(interactiveMode);
}

// Default is carrier arm
var interactiveFlag = 1;

// Flag code for the arm
var CARRIER_ARM_DEMO_CODE = 1;
var LASER_ARM_DEMO_CODE = 2;
var HAMMER_ARM_DEMO_CODE = 3;

// Flag for animation mode
var isInAnimationMode = false;

var polygon_mode;

var lighting_mode;
var lightWorldPosition;

var ANGLE_STEP = 3.0;     // The increments of rotation angle (degrees)
var g_arm1Angle = 90.0;   // The rotation angle of arm1 (degrees)
var g_joint1Angle = 0; // The rotation angle of joint1 (degrees)
var g_joint2Angle = 0.0;  // The rotation angle of joint2 (degrees)
var g_joint3Angle = 23.0;  // The rotation angle of joint3 (degrees)

var g_joint4Angle = 60.0; // For the arm part in object manufacture

// For easy access hammer arm joints
var structHammerArm = {
  ANGLE_STEP: 3.0,
  bodyJoint: 90, // Body spin joint
  middleJoint: 0, // Middle joint
  upperJoint: -23, // Upper joint
}

// For easy access laser arm joint
var structLaserArm = {
  ANGLE_STEP: 3.0,
  bodyJoint: 90, // Body spin joint
  middleJoint: -45, // Middle joint
  upperJoint: 0, // Upper joint
  wristJoint: -4, // Wrist spin joint
}

// For easy access carrier arm joints
var structCarrierArm = {
  ANGLE_STEP: 3.0,     // The increments of rotation angle (degrees)
  bodyJoint: 0,   // The rotation angle of arm1 (degrees)
  middleJoint: 0, // The rotation angle of joint1 (degrees)
  wristJoint: 0.0,  // The rotation angle of joint2 (degrees)
  fingerJoint: 0.0,  // The rotation angle of joint3 (degrees)
}

function keydown(ev, gl, o, viewProjMatrix, a_Position, cameraPosition, targetPosition) {

  // If in animation mode, this function can't be used
  if (isInAnimationMode) {
    return;
  }

  switch (ev.keyCode) {

    // To use camera lookat
    case 84: // T
      viewProjMatrix = new Matrix4();
      viewProjMatrix.setPerspective(50.0, gl.canvas.width / gl.canvas.height, 1.0, 100.0);
      cameraPosition.z += -1;
      viewProjMatrix.lookAt(
        cameraPosition.x, cameraPosition.y, cameraPosition.z,
        targetPosition.x, targetPosition.y, targetPosition.z,
        0.0, 1.0, 0.0
      );
      break;
    case 70: // F
      viewProjMatrix = new Matrix4();
      viewProjMatrix.setPerspective(50.0, gl.canvas.width / gl.canvas.height, 1.0, 100.0);
      cameraPosition.x += -1;
      viewProjMatrix.lookAt(
        cameraPosition.x, cameraPosition.y, cameraPosition.z,
        targetPosition.x, targetPosition.y, targetPosition.z,
        0.0, 1.0, 0.0
      );
      break;
    case 71: // G
      viewProjMatrix = new Matrix4();
      viewProjMatrix.setPerspective(50.0, gl.canvas.width / gl.canvas.height, 1.0, 100.0);
      cameraPosition.z += 1;
      viewProjMatrix.lookAt(
        cameraPosition.x, cameraPosition.y, cameraPosition.z,
        targetPosition.x, targetPosition.y, targetPosition.z,
        0.0, 1.0, 0.0
      );
      break;
    case 72: // H
      viewProjMatrix = new Matrix4();
      viewProjMatrix.setPerspective(50.0, gl.canvas.width / gl.canvas.height, 1.0, 100.0);
      cameraPosition.x += 1;
      viewProjMatrix.lookAt(
        cameraPosition.x, cameraPosition.y, cameraPosition.z,
        targetPosition.x, targetPosition.y, targetPosition.z,
        0.0, 1.0, 0.0
      );
      break;
    
    // To use camera standard
    case 83: // S 
      viewProjMatrix.translate(0, 0, -1);
      break;
    case 87: // W
      viewProjMatrix.translate(0, 0, 1);
      break;
    case 68: // D
      viewProjMatrix.translate(-1, 0, 0);
      break;
    case 65: // A
      viewProjMatrix.translate(1, 0, 0);
      break; a

    // To control joint in hierarchical object
    case 40: // Down arrow key -> the positive rotation of joint1 around the z-axis
      ev.view.event.preventDefault();
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE) {
        if (structCarrierArm.middleJoint < 135.0) {
          structCarrierArm.middleJoint += structCarrierArm.ANGLE_STEP;
        }
      }
      if (interactiveFlag == LASER_ARM_DEMO_CODE) {
        if (structLaserArm.middleJoint < 45.0) {
          structLaserArm.middleJoint += structLaserArm.ANGLE_STEP;
        }
      }
      break;
    case 38: // Up arrow key -> the negative rotation of joint1 around the z-axis
      ev.view.event.preventDefault();
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        if (structCarrierArm.middleJoint > -135.0) structCarrierArm.middleJoint -= ANGLE_STEP;
      if (interactiveFlag == LASER_ARM_DEMO_CODE) {
        if (structLaserArm.middleJoint > -45.0) {
          structLaserArm.middleJoint -= structLaserArm.ANGLE_STEP;
        }
      }
      break;
    case 39: // Right arrow key -> the positive rotation of arm1 around the y-axis
      ev.view.event.preventDefault();
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        structCarrierArm.bodyJoint = (structCarrierArm.bodyJoint + structCarrierArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        structHammerArm.bodyJoint = (structHammerArm.bodyJoint + structHammerArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == LASER_ARM_DEMO_CODE)
        structLaserArm.bodyJoint = (structLaserArm.bodyJoint + structLaserArm.ANGLE_STEP) % 360;
      break;
    case 37: // Left arrow key -> the negative rotation of arm1 around the y-axis
      ev.view.event.preventDefault();
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        structCarrierArm.bodyJoint = (structCarrierArm.bodyJoint - structCarrierArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        structHammerArm.bodyJoint = (structHammerArm.bodyJoint - structHammerArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == LASER_ARM_DEMO_CODE)
        structLaserArm.bodyJoint = (structLaserArm.bodyJoint - structLaserArm.ANGLE_STEP) % 360;
      break;
    case 90: // 'ï½š'key -> the positive rotation of joint2
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        structCarrierArm.wristJoint = (structCarrierArm.wristJoint + structCarrierArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        structHammerArm.middleJoint = (structHammerArm.middleJoint + structHammerArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == LASER_ARM_DEMO_CODE)
        structLaserArm.upperJoint = (structLaserArm.upperJoint + structLaserArm.ANGLE_STEP) % 360;
      break;
    case 88: // 'x'key -> the negative rotation of joint2
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        structCarrierArm.wristJoint = (structCarrierArm.wristJoint - structCarrierArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        structHammerArm.middleJoint = (structHammerArm.middleJoint - structHammerArm.ANGLE_STEP) % 360;
      else if (interactiveFlag == LASER_ARM_DEMO_CODE)
        structLaserArm.upperJoint = (structLaserArm.upperJoint - structLaserArm.ANGLE_STEP) % 360;
      break;
    case 86: // 'v'key -> the positive rotation of joint3
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        if (structCarrierArm.fingerJoint < 60.0) structCarrierArm.fingerJoint = (structCarrierArm.fingerJoint + structCarrierArm.ANGLE_STEP) % 360;
      if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        if (structHammerArm.upperJoint < 22.0) structHammerArm.upperJoint = (structHammerArm.upperJoint + structHammerArm.ANGLE_STEP) % 360;
      if (interactiveFlag == LASER_ARM_DEMO_CODE)
        if (structLaserArm.wristJoint < 60.0) structLaserArm.wristJoint = (structLaserArm.wristJoint + structLaserArm.ANGLE_STEP) % 360;
      break;
    case 67: // 'c'key -> the nagative rotation of joint3
      if (interactiveFlag == CARRIER_ARM_DEMO_CODE)
        if (structCarrierArm.fingerJoint > -60.0) structCarrierArm.fingerJoint = (structCarrierArm.fingerJoint - structCarrierArm.ANGLE_STEP) % 360;
      if (interactiveFlag == HAMMER_ARM_DEMO_CODE)
        if (structHammerArm.upperJoint > -22.0) structHammerArm.upperJoint = (structHammerArm.upperJoint - structHammerArm.ANGLE_STEP) % 360;
      if (interactiveFlag == LASER_ARM_DEMO_CODE)
        if (structLaserArm.wristJoint > -4.0) structLaserArm.wristJoint = (structLaserArm.wristJoint - structLaserArm.ANGLE_STEP) % 360;
      break;
    case 32: // 'Space'key -> change polygone mode
      ev.view.event.preventDefault();
      if (polygon_mode == gl.TRIANGLES) polygon_mode = gl.LINE_LOOP
      else polygon_mode = gl.TRIANGLES;
      break;
    case 9:  // 'tab'key -> change lighting mode
      ev.view.event.preventDefault();
      if (lighting_mode < 2) {
          // Directional lighting
          lighting_mode += 1;
          if (lighting_mode == 2) {
            lightWorldPosition = [12, 0, 252];
            gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
          }
      } else {
          // Point lighting
          lighting_mode = 0;
          lightWorldPosition = [20, 20, 0];
          gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
      }
      console.log(lighting_mode);
      gl.uniform1f(u_LightingMode, lighting_mode);
      break;
    case 73: // 'i'key -> move lighting to the front
      if (lighting_mode == 1) {
        lightWorldPosition[2] -= 2;
        gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
      }
      break;
    case 75: // 'k'key -> move lighting to the back
      if (lighting_mode == 1) {
        lightWorldPosition[2] += 2;
        gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
      }
      break;
    case 74: // 'j'key -> move lighting to the left
      if (lighting_mode == 1) {
        lightWorldPosition[0] -= 2;
        gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
      }
      break;
    case 76: // 'l'key -> move lighting to the right
      if (lighting_mode != 0) {
        lightWorldPosition[0] += 2;
        gl.uniform3fv(u_LightWorldPosition, lightWorldPosition);
      }
      break;
    // default: ; // Skip drawing at no effective action
  }

  // Assign to struct for easy access
  structDraw.gl = gl;
  structDraw.n = o;
  structDraw.viewProjMatrix = viewProjMatrix;
  structDraw.a_Position = a_Position;

  // Draw
  console.log(lightWorldPosition);
}


function interactiveMode() {
  var gl = structDraw.gl;
  var n = structDraw.n;
  var viewProjMatrix = structDraw.viewProjMatrix;
  var a_Position = structDraw.a_Position;
  draw(gl, n, viewProjMatrix, a_Position);
  if (isInAnimationMode) return;
  requestAnimationFrame(interactiveMode);
}

function animationMode(gl, o, viewProjMatrix, a_Position) {
  function render() {
    carrierArmAnimation();
    hammerArmAnimation();
    laserArmAnimation();
    draw(gl, o, viewProjMatrix, a_Position);
    if (!isInAnimationMode) return;
    requestAnimationFrame(render);
  }
  
  var flagRoboticArmJoint1 = true;
  function carrierArmAnimation() {
    var ANGLE_STEP = 5;
    if (structCarrierArm.middleJoint > -135.0 && flagRoboticArmJoint1) {
      structCarrierArm.middleJoint -= ANGLE_STEP;
    } else if (structCarrierArm.middleJoint < 80 && !flagRoboticArmJoint1) {
      structCarrierArm.middleJoint += ANGLE_STEP;
    } else {
      flagRoboticArmJoint1 = !flagRoboticArmJoint1;
    }
  }

  var flagHammerArmJoint1 = true;
  function hammerArmAnimation() {
    var ANGLE_STEP = 1.5;
    if (structHammerArm.upperJoint > -23.0 && flagHammerArmJoint1) {
      structHammerArm.upperJoint -= ANGLE_STEP;
    } else if (structHammerArm.upperJoint < 23 && !flagHammerArmJoint1) {
      structHammerArm.upperJoint += ANGLE_STEP;
    } else {
      flagHammerArmJoint1 = !flagHammerArmJoint1;
    }
  }

  function laserArmAnimation() {
    var ANGLE_STEP = 4;
    structLaserArm.upperJoint += ANGLE_STEP;
  }

  if (isInAnimationMode) {
    requestAnimationFrame(render);
  }
  else {
    requestAnimationFrame(interactiveMode);
  }
}

var g_envBuffer = null;    // Buffer object for floor & wall
var g_baseBuffer = null;     // Buffer object for a base
var g_arm1Buffer = null;     // Buffer object for arm1
var g_arm2Buffer = null;     // Buffer object for arm2
var g_palmBuffer = null;     // Buffer object for a palm
var g_fingerBuffer = null;   // Buffer object for fingers

// To become struct for easy access
var laserArm = null;
var hammerArm = null;
var assemblyLine = null;
var objectManufacture = null;

function initVertexBuffers(gl) {
  // Vertex coordinate (prepare coordinates of cuboids for all segments)
  var vertices_env = new Float32Array([ // Environment(50x0x50)
    -25.0, 0.0, -25.0, 25.0, 0.0, -25.0, 25.0, 0.0, 25.0, -25.0, 0.0, 25.0, // v0-v1-v2-v3 floor
    -25.0, 30.0, 25.0, -25.0, 30.0, -25.0, -25.0, 0.0, -25.0, -25.0, 0.0, 25.0, // v1-v6-v7-v2 left wall
    25.0, 0.0, -25.0, -25.0, 0.0, -25.0, -25.0, 30.0, -25.0, 25.0, 30.0, -25.0, // v1-v0-v4-v5 back wall
  ])

  // ---- FROM HERE ----
  var vertices_base = new Float32Array([ // Base(10x2x10)
    5.0, 2.0, 5.0, -5.0, 2.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
    5.0, 2.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 2.0, -5.0, // v0-v3-v4-v5 right
    5.0, 2.0, 5.0, 5.0, 2.0, -5.0, -5.0, 2.0, -5.0, -5.0, 2.0, 5.0, // v0-v5-v6-v1 up
    -5.0, 2.0, 5.0, -5.0, 2.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
    -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
    5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 2.0, -5.0, 5.0, 2.0, -5.0  // v4-v7-v6-v5 back
  ]);

  var vertices_arm1 = new Float32Array([  // Arm1(3x10x3)
    2.0, 10.0, 2.0, -2.0, 10.0, 2.0, -2.0, 0.0, 2.0, 2.0, 0.0, 2.0, // v0-v1-v2-v3 front
    2.0, 10.0, 2.0, 2.0, 0.0, 2.0, 2.0, 0.0, -2.0, 2.0, 10.0, -2.0, // v0-v3-v4-v5 right
    2.0, 10.0, 2.0, 2.0, 10.0, -2.0, -2.0, 10.0, -2.0, -2.0, 10.0, 2.0, // v0-v5-v6-v1 up
    -2.0, 10.0, 2.0, -2.0, 10.0, -2.0, -2.0, 0.0, -2.0, -2.0, 0.0, 2.0, // v1-v6-v7-v2 left
    -2.0, 0.0, -2.0, 2.0, 0.0, -2.0, 2.0, 0.0, 2.0, -2.0, 0.0, 2.0, // v7-v4-v3-v2 down
    2.0, 0.0, -2.0, -2.0, 0.0, -2.0, -2.0, 10.0, -2.0, 2.0, 10.0, -2.0  // v4-v7-v6-v5 back
  ]);

  var vertices_arm2 = new Float32Array([  // Arm2(4x10x4)
    1.5, 10.0, 1.5, -1.5, 10.0, 1.5, -1.5, 0.0, 1.5, 1.5, 0.0, 1.5, // v0-v1-v2-v3 front
    1.5, 10.0, 1.5, 1.5, 0.0, 1.5, 1.5, 0.0, -1.5, 1.5, 10.0, -1.5, // v0-v3-v4-v5 right
    1.5, 10.0, 1.5, 1.5, 10.0, -1.5, -1.5, 10.0, -1.5, -1.5, 10.0, 1.5, // v0-v5-v6-v1 up
    -1.5, 10.0, 1.5, -1.5, 10.0, -1.5, -1.5, 0.0, -1.5, -1.5, 0.0, 1.5, // v1-v6-v7-v2 left
    -1.5, 0.0, -1.5, 1.5, 0.0, -1.5, 1.5, 0.0, 1.5, -1.5, 0.0, 1.5, // v7-v4-v3-v2 down
    1.5, 0.0, -1.5, -1.5, 0.0, -1.5, -1.5, 10.0, -1.5, 1.5, 10.0, -1.5  // v4-v7-v6-v5 back
  ]);

  var vertices_palm = new Float32Array([  // Palm(2x2x6)
    3.0, 2.0, 3.0, -3.0, 2.0, 3.0, -3.0, 0.0, 3.0, 3.0, 0.0, 3.0, // v0-v1-v2-v3 front
    3.0, 2.0, 3.0, 3.0, 0.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, -3.0, // v0-v3-v4-v5 right
    3.0, 2.0, 3.0, 3.0, 2.0, -3.0, -3.0, 2.0, -3.0, -3.0, 2.0, 3.0, // v0-v5-v6-v1 up
    -3.0, 2.0, 3.0, -3.0, 2.0, -3.0, -3.0, 0.0, -3.0, -3.0, 0.0, 3.0, // v1-v6-v7-v2 left
    -3.0, 0.0, -3.0, 3.0, 0.0, -3.0, 3.0, 0.0, 3.0, -3.0, 0.0, 3.0, // v7-v4-v3-v2 down
    3.0, 0.0, -3.0, -3.0, 0.0, -3.0, -3.0, 2.0, -3.0, 3.0, 2.0, -3.0  // v4-v7-v6-v5 back
  ]);

  var vertices_finger = new Float32Array([  // Fingers(1x2x1)
    0.5, 2.0, 0.5, -0.5, 2.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, 0.5, // v0-v1-v2-v3 front
    0.5, 2.0, 0.5, 0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 2.0, -0.5, // v0-v3-v4-v5 right
    0.5, 2.0, 0.5, 0.5, 2.0, -0.5, -0.5, 2.0, -0.5, -0.5, 2.0, 0.5, // v0-v5-v6-v1 up
    -0.5, 2.0, 0.5, -0.5, 2.0, -0.5, -0.5, 0.0, -0.5, -0.5, 0.0, 0.5, // v1-v6-v7-v2 left
    -0.5, 0.0, -0.5, 0.5, 0.0, -0.5, 0.5, 0.0, 0.5, -0.5, 0.0, 0.5, // v7-v4-v3-v2 down
    0.5, 0.0, -0.5, -0.5, 0.0, -0.5, -0.5, 2.0, -0.5, 0.5, 2.0, -0.5  // v4-v7-v6-v5 back
  ]);

  // --- TO HERE --- is to create the carrier arm

  // Normal
  var normals = new Float32Array([
    0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, // v0-v1-v2-v3 front
    1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, // v0-v3-v4-v5 right
    0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, // v0-v5-v6-v1 up
    -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, // v1-v6-v7-v2 left
    0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, // v7-v4-v3-v2 down
    0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0  // v4-v7-v6-v5 back
  ]);

  laserArm = {
    vertices_base: new Float32Array([ // Base(10x2x10)
      5.0, 2.0, 5.0, -5.0, 2.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
      5.0, 2.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 2.0, -5.0, // v0-v3-v4-v5 right
      5.0, 2.0, 5.0, 5.0, 2.0, -5.0, -5.0, 2.0, -5.0, -5.0, 2.0, 5.0, // v0-v5-v6-v1 up
      -5.0, 2.0, 5.0, -5.0, 2.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
      -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
      5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 2.0, -5.0, 5.0, 2.0, -5.0  // v4-v7-v6-v5 back
    ]),
    vertices_arm1: new Float32Array([  // Arm1(3x10x3)
      2.0, 10.0, 2.0, -2.0, 10.0, 2.0, -2.0, 0.0, 2.0, 2.0, 0.0, 2.0, // v0-v1-v2-v3 front
      2.0, 10.0, 2.0, 2.0, 0.0, 2.0, 2.0, 0.0, -2.0, 2.0, 10.0, -2.0, // v0-v3-v4-v5 right
      2.0, 10.0, 2.0, 2.0, 10.0, -2.0, -2.0, 10.0, -2.0, -2.0, 10.0, 2.0, // v0-v5-v6-v1 up
      -2.0, 10.0, 2.0, -2.0, 10.0, -2.0, -2.0, 0.0, -2.0, -2.0, 0.0, 2.0, // v1-v6-v7-v2 left
      -2.0, 0.0, -2.0, 2.0, 0.0, -2.0, 2.0, 0.0, 2.0, -2.0, 0.0, 2.0, // v7-v4-v3-v2 down
      2.0, 0.0, -2.0, -2.0, 0.0, -2.0, -2.0, 10.0, -2.0, 2.0, 10.0, -2.0  // v4-v7-v6-v5 back
    ]),
    vertices_arm2: new Float32Array([  // Arm2(4x10x4)
      1.5, 10.0, 1.5, -1.5, 10.0, 1.5, -1.5, 0.0, 1.5, 1.5, 0.0, 1.5, // v0-v1-v2-v3 front
      1.5, 10.0, 1.5, 1.5, 0.0, 1.5, 1.5, 0.0, -1.5, 1.5, 10.0, -1.5, // v0-v3-v4-v5 right
      1.5, 10.0, 1.5, 1.5, 10.0, -1.5, -1.5, 10.0, -1.5, -1.5, 10.0, 1.5, // v0-v5-v6-v1 up
      -1.5, 10.0, 1.5, -1.5, 10.0, -1.5, -1.5, 0.0, -1.5, -1.5, 0.0, 1.5, // v1-v6-v7-v2 left
      -1.5, 0.0, -1.5, 1.5, 0.0, -1.5, 1.5, 0.0, 1.5, -1.5, 0.0, 1.5, // v7-v4-v3-v2 down
      1.5, 0.0, -1.5, -1.5, 0.0, -1.5, -1.5, 10.0, -1.5, 1.5, 10.0, -1.5  // v4-v7-v6-v5 back
    ]),
    vertices_palm: new Float32Array([  // Palm(2x2x6)
      3.0, 2.0, 3.0, -3.0, 2.0, 3.0, -3.0, 0.0, 3.0, 3.0, 0.0, 3.0, // v0-v1-v2-v3 front
      3.0, 2.0, 3.0, 3.0, 0.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, -3.0, // v0-v3-v4-v5 right
      3.0, 2.0, 3.0, 3.0, 2.0, -3.0, -3.0, 2.0, -3.0, -3.0, 2.0, 3.0, // v0-v5-v6-v1 up
      -3.0, 2.0, 3.0, -3.0, 2.0, -3.0, -3.0, 0.0, -3.0, -3.0, 0.0, 3.0, // v1-v6-v7-v2 left
      -3.0, 0.0, -3.0, 3.0, 0.0, -3.0, 3.0, 0.0, 3.0, -3.0, 0.0, 3.0, // v7-v4-v3-v2 down
      3.0, 0.0, -3.0, -3.0, 0.0, -3.0, -3.0, 2.0, -3.0, 3.0, 2.0, -3.0  // v4-v7-v6-v5 back
    ]),
    vertices_finger: new Float32Array([  // Fingers(1x2x1)
      0.5, 7.0, 0.5, -0.5, 7.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, 0.5, // v0-v1-v2-v3 front
      0.5, 7.0, 0.5, 0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 7.0, -0.5, // v0-v3-v4-v5 right
      0.5, 7.0, 0.5, 0.5, 7.0, -0.5, -0.5, 7.0, -0.5, -0.5, 7.0, 0.5, // v0-v5-v6-v1 up
      -0.5, 7.0, 0.5, -0.5, 7.0, -0.5, -0.5, 0.0, -0.5, -0.5, 0.0, 0.5, // v1-v6-v7-v2 left
      -0.5, 0.0, -0.5, 0.5, 0.0, -0.5, 0.5, 0.0, 0.5, -0.5, 0.0, 0.5, // v7-v4-v3-v2 down
      0.5, 0.0, -0.5, -0.5, 0.0, -0.5, -0.5, 7.0, -0.5, 0.5, 7.0, -0.5  // v4-v7-v6-v5 back
    ]),
    vertices_finger_bottom: new Float32Array([  // Fingers(1x2x1)
      0.75, 4.0, 0.75, -0.75, 4.0, 0.75, -0.75, 0.0, 0.75, 0.75, 0.0, 0.75, // v0-v1-v2-v3 front
      0.75, 4.0, 0.75, 0.75, 0.0, 0.75, 0.75, 0.0, -0.75, 0.75, 4.0, -0.75, // v0-v3-v4-v5 right
      0.75, 4.0, 0.75, 0.75, 4.0, -0.75, -0.75, 4.0, -0.75, -0.75, 4.0, 0.75, // v0-v5-v6-v1 up
      -0.75, 4.0, 0.75, -0.75, 4.0, -0.75, -0.75, 0.0, -0.75, -0.75, 0.0, 0.75, // v1-v6-v7-v2 left
      -0.75, 0.0, -0.75, 0.75, 0.0, -0.75, 0.75, 0.0, 0.75, -0.75, 0.0, 0.75, // v7-v4-v3-v2 down
      0.75, 0.0, -0.75, -0.75, 0.0, -0.75, -0.75, 4.0, -0.75, 0.75, 4.0, -0.75  // v4-v7-v6-v5 back
    ]),
    g_baseBuffer: null,     // Buffer object for a base
    g_arm1Buffer: null,     // Buffer object for arm1
    g_arm2Buffer: null,     // Buffer object for arm2
    g_palmBuffer: null,     // Buffer object for a palm
    g_fingerBuffer: null,   // Buffer object for fingers
    g_fingerBottomBuffer: null,
  }

  hammerArm = {
    vertices_base: new Float32Array([ // Base(10x2x10)
      5.0, 2.0, 5.0, -5.0, 2.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
      5.0, 2.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 2.0, -5.0, // v0-v3-v4-v5 right
      5.0, 2.0, 5.0, 5.0, 2.0, -5.0, -5.0, 2.0, -5.0, -5.0, 2.0, 5.0, // v0-v5-v6-v1 up
      -5.0, 2.0, 5.0, -5.0, 2.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
      -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
      5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 2.0, -5.0, 5.0, 2.0, -5.0  // v4-v7-v6-v5 back
    ]),
    vertices_arm1: new Float32Array([  // Arm1(3x10x3)
      3.0, 10.0, 3.0, -3.0, 10.0, 3.0, -3.0, 0.0, 3.0, 3.0, 0.0, 3.0, // v0-v1-v2-v3 front
      3.0, 10.0, 3.0, 3.0, 0.0, 3.0, 3.0, 0.0, -3.0, 3.0, 10.0, -3.0, // v0-v3-v4-v5 right
      3.0, 10.0, 3.0, 3.0, 10.0, -3.0, -3.0, 10.0, -3.0, -3.0, 10.0, 3.0, // v0-v5-v6-v1 up
      -3.0, 10.0, 3.0, -3.0, 10.0, -3.0, -3.0, 0.0, -3.0, -3.0, 0.0, 3.0, // v1-v6-v7-v2 left
      -3.0, 0.0, -3.0, 3.0, 0.0, -3.0, 3.0, 0.0, 3.0, -3.0, 0.0, 3.0, // v7-v4-v3-v2 down
      3.0, 0.0, -3.0, -3.0, 0.0, -3.0, -3.0, 10.0, -3.0, 3.0, 10.0, -3.0  // v4-v7-v6-v5 back
    ]),
    vertices_arm2: new Float32Array([  // Arm2(4x10x4)
      2, 10.0, 2, -2, 10.0, 2, -2, 0.0, 2, 2, 0.0, 2, // v0-v1-v2-v3 front
      2, 10.0, 2, 2, 0.0, 2, 2, 0.0, -2, 2, 10.0, -2, // v0-v3-v4-v5 right
      2, 10.0, 2, 2, 10.0, -2, -2, 10.0, -2, -1.5, 10.0, 2, // v0-v5-v6-v1 up
      -2, 10.0, 2, -2, 10.0, -2, -2, 0.0, -2, -1.5, 0.0, 2, // v1-v6-v7-v2 left
      -2, 0.0, -2, 2, 0.0, -2, 2, 0.0, 2, -2, 0.0, 2, // v7-v4-v3-v2 down
      2, 0.0, -2, -2, 0.0, -2, -2, 10.0, -2, 2, 10.0, -2  // v4-v7-v6-v5 back
    ]),
    vertices_palm: new Float32Array([  // Palm(2x2x6)
      5.0, 4.0, 5.0, -5.0, 4.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
      5.0, 4.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 4.0, -5.0, // v0-v3-v4-v5 right
      5.0, 4.0, 5.0, 5.0, 4.0, -5.0, -5.0, 4.0, -5.0, -5.0, 4.0, 5.0, // v0-v5-v6-v1 up
      -5.0, 4.0, 5.0, -5.0, 4.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
      -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
      5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 4.0, -5.0, 5.0, 4.0, -5.0  // v4-v7-v6-v5 back
    ]),
    vertices_finger_bottom: new Float32Array([  // Fingers(1x2x1)
      1.5, 6.0, 1.5, -1.5, 6.0, 1.5, -1.5, 0.0, 1.5, 1.5, 0.0, 1.5, // v0-v1-v2-v3 front
      1.5, 6.0, 1.5, 1.5, 0.0, 1.5, 1.5, 0.0, -1.5, 1.5, 6.0, -1.5, // v0-v3-v4-v5 right
      1.5, 6.0, 1.5, 1.5, 6.0, -1.5, -1.5, 6.0, -1.5, -1.5, 6.0, 1.5, // v0-v5-v6-v1 up
      -1.5, 6.0, 1.5, -1.5, 6.0, -1.5, -1.5, 0.0, -1.5, -1.5, 0.0, 1.5, // v1-v6-v7-v2 left
      -1.5, 0.0, -1.5, 1.5, 0.0, -1.5, 1.5, 0.0, 1.5, -1.5, 0.0, 1.5, // v7-v4-v3-v2 down
      1.5, 0.0, -1.5, -1.5, 0.0, -1.5, -1.5, 6.0, -1.5, 1.5, 6.0, -1.5  // v4-v7-v6-v5 back
    ]),
    vertices_finger: new Float32Array([  // Fingers(1x2x1)
      0.80, 7.0, 0.80, -0.80, 7.0, 0.80, -0.80, 0.0, 0.80, 0.5, 0.0, 0.80, // v0-v1-v2-v3 front
      0.80, 7.0, 0.80, 0.80, 0.0, 0.80, 0.80, 0.0, -0.80, 0.5, 7.0, -0.80, // v0-v3-v4-v5 right
      0.80, 7.0, 0.80, 0.80, 7.0, -0.80, -0.80, 7.0, -0.80, -0.80, 7.0, 0.80, // v0-v5-v6-v1 up
      -0.80, 7.0, 0.80, -0.80, 7.0, -0.80, -0.80, 0.0, -0.80, -0.80, 0.0, 0.80, // v1-v6-v7-v2 left
      -0.80, 0.0, -0.80, 0.80, 0.0, -0.80, 0.80, 0.0, 0.80, -0.80, 0.0, 0.80, // v7-v4-v3-v2 down
      0.80, 0.0, -0.80, -0.80, 0.0, -0.80, -0.80, 7.0, -0.80, 0.80, 7.0, -0.80  // v4-v7-v6-v5 back
    ]),
    vertices_finger2: new Float32Array([  // Fingers(1x2x1)
      0.5, 7.0, 0.5, -0.5, 7.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, 0.5, // v0-v1-v2-v3 front
      0.5, 7.0, 0.5, 0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 7.0, -0.5, // v0-v3-v4-v5 right
      0.5, 7.0, 0.5, 0.5, 7.0, -0.5, -0.5, 7.0, -0.5, -0.5, 7.0, 0.5, // v0-v5-v6-v1 up
      -0.5, 7.0, 0.5, -0.5, 7.0, -0.5, -0.5, 0.0, -0.5, -0.5, 0.0, 0.5, // v1-v6-v7-v2 left
      -0.5, 0.0, -0.5, 0.5, 0.0, -0.5, 0.5, 0.0, 0.5, -0.5, 0.0, 0.5, // v7-v4-v3-v2 down
      0.5, 0.0, -0.5, -0.5, 0.0, -0.5, -0.5, 7.0, -0.5, 0.5, 7.0, -0.5  // v4-v7-v6-v5 back
    ]),
    vertices_balok: new Float32Array([  // Fingers(1x2x1)
      1.5, 3.0, 1.5, -1.5, 3.0, 1.5, -1.5, 0.0, 1.5, 1.5, 0.0, 1.5, // v0-v1-v2-v3 front
      1.5, 3.0, 1.5, 1.5, 0.0, 1.5, 1.5, 0.0, -1.5, 1.5, 3.0, -1.5, // v0-v3-v4-v5 right
      1.5, 3.0, 1.5, 1.5, 3.0, -1.5, -1.5, 3.0, -1.5, -1.5, 3.0, 1.5, // v0-v5-v6-v1 up
      -1.5, 3.0, 1.5, -1.5, 3.0, -1.5, -1.5, 0.0, -1.5, -1.5, 0.0, 1.5, // v1-v6-v7-v2 left
      -1.5, 0.0, -1.5, 1.5, 0.0, -1.5, 1.5, 0.0, 1.5, -1.5, 0.0, 1.5, // v7-v4-v3-v2 down
      1.5, 0.0, -1.5, -1.5, 0.0, -1.5, -1.5, 3.0, -1.5, 1.5, 3.0, -1.5  // v4-v7-v6-v5 back
    ]),
    g_baseBuffer: null,     // Buffer object for a base
    g_arm1Buffer: null,     // Buffer object for arm1
    g_arm2Buffer: null,     // Buffer object for arm2
    g_palmBuffer: null,     // Buffer object for a palm
    g_fingerBuffer: null,   // Buffer object for fingers
    g_finger2Buffer: null,
    g_balokBuffer: null,
    g_fingerBottomBuffer: null,
  }

  assemblyLine = {
    vertices_base: new Float32Array([ // Base(10x2x10)
      5.0, 10.0, 5.0, -5.0, 10.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
      5.0, 10.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 10, -5.0, // v0-v3-v4-v5 right
      5.0, 10.0, 5.0, 5.0, 10.0, -5.0, -5.0, 10.0, -5.0, -5.0, 10.0, 5.0, // v0-v5-v6-v1 up
      -5.0, 10.0, 5.0, -5.0, 10.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
      -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
      5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 10.0, -5.0, 5.0, 10.0, -5.0,  // v4-v7-v6-v5 back
    ]),

    g_baseBuffer: null,     // Buffer object for a base
  }

  objectManufacture = {
    vertices_base: new Float32Array([ // Base(10x2x10)
      5.0, 10.0, 5.0, -5.0, 10.0, 5.0, -5.0, 0.0, 5.0, 5.0, 0.0, 5.0, // v0-v1-v2-v3 front
      5.0, 10.0, 5.0, 5.0, 0.0, 5.0, 5.0, 0.0, -5.0, 5.0, 2.0, -5.0, // v0-v3-v4-v5 right
      5.0, 10.0, 5.0, 5.0, 10.0, -5.0, -5.0, 10.0, -5.0, -5.0, 10.0, 5.0, // v0-v5-v6-v1 up
      -5.0, 10.0, 5.0, -5.0, 10.0, -5.0, -5.0, 0.0, -5.0, -5.0, 0.0, 5.0, // v1-v6-v7-v2 left
      -5.0, 0.0, -5.0, 5.0, 0.0, -5.0, 5.0, 0.0, 5.0, -5.0, 0.0, 5.0, // v7-v4-v3-v2 down
      5.0, 0.0, -5.0, -5.0, 0.0, -5.0, -5.0, 10.0, -5.0, 5.0, 10.0, -5.0,  // v4-v7-v6-v5 back
    ]),

    vertices_palm: new Float32Array([  // Palm(2x2x6)
      10.0, 4.0, 10.0, -10.0, 4.0, 10.0, -10.0, 0.0, 10.0, 10.0, 0.0, 10.0, // v0-v1-v2-v3 front
      10.0, 4.0, 10.0, 10.0, 0.0, 10.0, 10.0, 0.0, -10.0, 10.0, 4.0, -10.0, // v0-v3-v4-v5 right
      10.0, 4.0, 10.0, 10.0, 4.0, -10.0, -10.0, 4.0, -10.0, -10.0, 4.0, 10.0, // v0-v5-v6-v1 up
      -10.0, 4.0, 10.0, -10.0, 4.0, -10.0, -10.0, 0.0, -10.0, -10.0, 0.0, 10.0, // v1-v6-v7-v2 left
      -10.0, 0.0, -10.0, 10.0, 0.0, -10.0, 10.0, 0.0, 10.0, -10.0, 0.0, 10.0, // v7-v4-v3-v2 down
      10.0, 0.0, -10.0, -10.0, 0.0, -10.0, -10.0, 4.0, -10.0, 10.0, 4.0, -10.0  // v4-v7-v6-v5 back
    ]),

    g_baseBuffer: null,     // Buffer object for a base
    g_palmBuffer: null,
  }

  // Indices of the vertices
  var indices = new Uint8Array([
    0, 1, 2, 0, 2, 3,    // front
    4, 5, 6, 4, 6, 7,    // right
    8, 9, 10, 8, 10, 11,    // up
    12, 13, 14, 12, 14, 15,    // left
    16, 17, 18, 16, 18, 19,    // down
    20, 21, 22, 20, 22, 23     // back
  ]);

  // Write coords to buffers, but don't assign to attribute variables
  g_envBuffer = initArrayBufferForLaterUse(gl, vertices_env, 3, gl.FLOAT);
  g_baseBuffer = initArrayBufferForLaterUse(gl, vertices_base, 3, gl.FLOAT);
  g_arm1Buffer = initArrayBufferForLaterUse(gl, vertices_arm1, 3, gl.FLOAT);
  g_arm2Buffer = initArrayBufferForLaterUse(gl, vertices_arm2, 3, gl.FLOAT);
  g_palmBuffer = initArrayBufferForLaterUse(gl, vertices_palm, 3, gl.FLOAT);
  g_fingerBuffer = initArrayBufferForLaterUse(gl, vertices_finger, 3, gl.FLOAT);

  laserArm.g_baseBuffer = initArrayBufferForLaterUse(gl, laserArm.vertices_base, 3, gl.FLOAT);
  laserArm.g_arm1Buffer = initArrayBufferForLaterUse(gl, laserArm.vertices_arm1, 3, gl.FLOAT);
  laserArm.g_arm2Buffer = initArrayBufferForLaterUse(gl, laserArm.vertices_arm2, 3, gl.FLOAT);
  laserArm.g_palmBuffer = initArrayBufferForLaterUse(gl, laserArm.vertices_palm, 3, gl.FLOAT);
  laserArm.g_fingerBuffer = initArrayBufferForLaterUse(gl, laserArm.vertices_finger, 3, gl.FLOAT);
  laserArm.g_fingerBottomBuffer = initArrayBufferForLaterUse(gl, laserArm.vertices_finger_bottom, 3, gl.FLOAT);

  hammerArm.g_baseBuffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_base, 3, gl.FLOAT);
  hammerArm.g_arm1Buffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_arm1, 3, gl.FLOAT);
  hammerArm.g_arm2Buffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_arm2, 3, gl.FLOAT);
  hammerArm.g_palmBuffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_palm, 3, gl.FLOAT);
  hammerArm.g_fingerBuffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_finger, 3, gl.FLOAT);
  hammerArm.g_finger2Buffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_finger2, 3, gl.FLOAT);
  hammerArm.g_balokBuffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_balok, 3, gl.FLOAT);
  hammerArm.g_fingerBottomBuffer = initArrayBufferForLaterUse(gl, hammerArm.vertices_finger_bottom, 3, gl.FLOAT);

  assemblyLine.g_baseBuffer = initArrayBufferForLaterUse(gl, assemblyLine.vertices_base, 3, gl.FLOAT);

  objectManufacture.g_baseBuffer = initArrayBufferForLaterUse(gl, objectManufacture.vertices_base, 3, gl.FLOAT);
  objectManufacture.g_palmBuffer = initArrayBufferForLaterUse(gl, objectManufacture.vertices_palm, 3, gl.FLOAT);

  // Write normals to a buffer, assign it to a_Normal and enable it
  if (!initArrayBuffer(gl, 'a_Normal', normals, 3, gl.FLOAT)) return -1;

  // Write the indices to the buffer object
  var indexBuffer = gl.createBuffer();
  if (!indexBuffer) {
    console.log('Failed to create the buffer object');
    return -1;
  }
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

  return indices.length;
}

function initArrayBufferForLaterUse(gl, data, num, type) {
  var buffer = gl.createBuffer();   // Create a buffer object
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return null;
  }
  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

  // Store the necessary information to assign the object to the attribute variable later
  buffer.num = num;
  buffer.type = type;

  return buffer;
}

function initArrayBuffer(gl, attribute, data, num, type) {
  var buffer = gl.createBuffer();   // Create a buffer object
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

  // Assign the buffer object to the attribute variable
  var a_attribute = gl.getAttribLocation(gl.program, attribute);
  if (a_attribute < 0) {
    console.log('Failed to get the storage location of ' + attribute);
    return false;
  }
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_attribute);

  return true;
}


// Coordinate transformation matrix
var g_modelMatrix = new Matrix4(), g_mvpMatrix = new Matrix4();

var structDraw = {
  gl: null,
  n: null,
  viewProjMatrix: null,
  a_Position: null
}

function draw(gl, n, viewProjMatrix, a_Position) {
  // Clear color and depth buffer
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // To make the floor fit to viewer view
  g_modelMatrix.setTranslate(0.0, -12.0, 0.0);

  //Draw Environment
  drawSegment(gl, n, g_envBuffer, viewProjMatrix, a_Position, 1);

  drawObjectManufacture(gl, n, viewProjMatrix, a_Position, 7);

  drawAssemblyLine(gl, n, viewProjMatrix, a_Position, 6);

  drawHammerArm(gl, n, viewProjMatrix, a_Position, 3);

  drawLaserArm(gl, n, viewProjMatrix, a_Position, 3);

  drawCarrierArm(gl, n, viewProjMatrix, a_Position, 3);
}

var g_matrixStack = []; // Array for storing a matrix
function pushMatrix(m) { // Store the specified matrix to the array
  var m2 = new Matrix4(m);
  g_matrixStack.push(m2);
}


function popMatrix() { // Retrieve the matrix from the array
  return g_matrixStack.pop();
}

var g_normalMatrix = new Matrix4();  // Coordinate transformation matrix for normals

// Draw segments
function drawSegment(gl, n, buffer, viewProjMatrix, a_Position, texturetype) {
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  // Assign the buffer object to the attribute variable
  gl.vertexAttribPointer(a_Position, buffer.num, buffer.type, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_Position);
  
  setTexcoords(gl);
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureBuffer);
  // Assign the buffer object to the attribute variable
  gl.vertexAttribPointer(a_texcoord, cubeTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_texcoord);

  gl.uniform1i(gl.getUniformLocation(gl.program, "thetexture"), texturetype);

  // Calculate the model view project matrix and pass it to u_MvpMatrix
  g_mvpMatrix.set(viewProjMatrix);
  g_mvpMatrix.multiply(g_modelMatrix);
  gl.uniformMatrix4fv(u_MvpMatrix, false, g_mvpMatrix.elements);
  // Calculate matrix for normal and pass it to u_NormalMatrix
  g_normalMatrix.setInverseOf(g_modelMatrix);
  g_normalMatrix.transpose();
  gl.uniformMatrix4fv(u_NormalMatrix, false, g_normalMatrix.elements);

  // Draw
  gl.drawElements(polygon_mode, n, gl.UNSIGNED_BYTE, 0);
}

function configureTexture(gl, image, textureno) {
  var texture = gl.createTexture();
  gl.activeTexture(textureno);
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
  gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB,
       gl.RGB, gl.UNSIGNED_BYTE, image );
  gl.generateMipmap( gl.TEXTURE_2D );
  gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                    gl.NEAREST_MIPMAP_LINEAR );
  gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );  
}

function initTexture(gl) {
  var image0 = new Image();
  image0.onload = function() {
     configureTexture(gl, image0, gl.TEXTURE0);
  }
  image0.src = "img/arm_texture2.jpg"
  
  var image1 = new Image();
  image1.onload = function() {
     configureTexture(gl, image1, gl.TEXTURE1);
  }
  image1.src = "img/wall2.jpg"
  
  var image2 = new Image();
  image2.onload = function() {
     configureTexture(gl, image2, gl.TEXTURE2);
  }
  image2.src = "img/metal.jpg"
  
  var image3 = new Image();
  image3.onload = function() {
     configureTexture(gl, image3, gl.TEXTURE3);
  }
  image3.src = "img/jason-leung-unsplash.png"
  
  var image6 = new Image();
  image6.onload = function() {
     configureTexture(gl, image6, gl.TEXTURE6);
  }
  image6.src = "img/black.jpg"
  
  var image7 = new Image();
  image7.onload = function() {
     configureTexture(gl, image7, gl.TEXTURE7);
  }
  image7.src = "img/jj-ying-unsplash.png"
  
  var image8 = new Image();
  image8.onload = function() {
     configureTexture(gl, image8, gl.TEXTURE8);
  }
  image8.src = "img/glass.jpg"
}

var cubeTextureBuffer;

// Fill the buffer with texture coordinates the cube.
function setTexcoords(gl) {
  var textureCubeCoords = [
    // Front face
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,

    // Back face
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,

    // Top face
    0.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,

    // Bottom face
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,

    // Right face
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,

    // Left face
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
  ];
  cubeTextureBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeTextureBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCubeCoords), gl.STATIC_DRAW);
  cubeTextureBuffer.itemSize = 2;
  cubeTextureBuffer.numItems = 24;
}

function drawObjectManufacture(gl, n, viewProjMatrix, a_Position, texturetype) {
  // Draw a base
  var baseHeight = 10.0;
  g_modelMatrix.setTranslate(0, -11, 20);
  g_modelMatrix.scale(0.4, 0.4, 0.4);
  // drawSegment(gl, n, objectManufacture.g_baseBuffer, viewProjMatrix, a_Position);

  // Palm
  var palm1Length = 2.0;
  g_modelMatrix.translate(0.0, baseHeight, 0.0);       // Move to palm
  g_modelMatrix.rotate(g_joint2Angle, 0.0, 1.0, 0.0);  // Rotate around the y-axis
  drawSegment(gl, n, objectManufacture.g_palmBuffer, viewProjMatrix, a_Position, texturetype);  // Draw

  // Move to the center of the tip of the palm1
  g_modelMatrix.translate(0.0, palm1Length, 0.0);

  // Finger1
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(7.0, 0.0, 0.0);
  g_modelMatrix.rotate(g_joint3Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 top2
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_finger2Buffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 balok
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_balokBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger2
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0.0, 0.0, -7.0);
  g_modelMatrix.rotate(-g_joint3Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger2 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger2 top2
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_finger2Buffer, viewProjMatrix, a_Position, texturetype);

  // Finger2 balok
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(-g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_balokBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger3
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(-7.0, 0.0, 0.0);
  g_modelMatrix.rotate(-g_joint3Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger3 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger3 top2
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_finger2Buffer, viewProjMatrix, a_Position, texturetype);

  // Finger3 balok
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 0.0, 0.0, 1.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_balokBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger4
  g_modelMatrix.translate(0.0, 0.0, 7.0);
  g_modelMatrix.rotate(g_joint3Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger4 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger4 top2
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_finger2Buffer, viewProjMatrix, a_Position, texturetype);

  // Finger4 balok
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(g_joint4Angle, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_balokBuffer, viewProjMatrix, a_Position, texturetype);
}

function drawAssemblyLine(gl, n, viewProjMatrix, a_Position, texturetype) {
  // Draw a base
  g_modelMatrix.setTranslate(0.0, -12, 0);
  g_modelMatrix.scale(0.8, 0.5, 5);
  drawSegment(gl, n, assemblyLine.g_baseBuffer, viewProjMatrix, a_Position, texturetype);

}

function drawHammerArm(gl, n, viewProjMatrix, a_Position, texturetype) {
  // Draw a base
  var baseHeight = 2.0;
  g_modelMatrix.setTranslate(-9.5, -12, -2);
  g_modelMatrix.scale(0.4, 0.4, 0.4);
  drawSegment(gl, n, hammerArm.g_baseBuffer, viewProjMatrix, a_Position, texturetype);

  // Arm1
  var arm1Length = 0.0;
  g_modelMatrix.translate(0.0, baseHeight, 0.0);     // Move onto the base
  g_modelMatrix.rotate(structHammerArm.bodyJoint, 0.0, 1.0, 0.0);  // Rotate around the y-axis
  g_modelMatrix.scale(1.0, 1.5, 1.0);  // Rotate around the y-axis
  g_modelMatrix.translate(0.0, arm1Length, 0.0);       // Move to joint1
  g_modelMatrix.rotate(structHammerArm.middleJoint, 0.0, 1.0, 0.0);  // Rotate around the z-axis
  g_modelMatrix.scale(1, 1 / 1.5, 1);
  g_modelMatrix.rotate(structHammerArm.middleJoint, 0.0, 1.0, 0.0);  // Rotate around the y-axis


  // Finger1
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0, 0.0, 0.0);
  g_modelMatrix.rotate(structHammerArm.upperJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  g_modelMatrix.scale(1.5, 1.5, 1.5);
  drawSegment(gl, n, hammerArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 top
  g_modelMatrix.translate(0.0, 5.0, 0.0);
  g_modelMatrix.rotate(structHammerArm.upperJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 top2
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(structHammerArm.upperJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, hammerArm.g_finger2Buffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 balok
  g_modelMatrix.translate(0.0, 7.0, 0.0);
  g_modelMatrix.rotate(structHammerArm.upperJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  g_modelMatrix.scale(2.0, 2, 3.0);
  drawSegment(gl, n, hammerArm.g_balokBuffer, viewProjMatrix, a_Position, texturetype);
}

function drawLaserArm(gl, n, viewProjMatrix, a_Position, texturetype) {
  // Draw a base
  var baseHeight = 2.0;
  g_modelMatrix.setTranslate(15.0, -12, 12);
  g_modelMatrix.scale(0.4, 0.4, 0.4);
  drawSegment(gl, n, laserArm.g_baseBuffer, viewProjMatrix, a_Position, texturetype);

  // Arm1
  var arm1Length = 10.0;
  g_modelMatrix.translate(0.0, baseHeight, 0.0);     // Move onto the base
  g_modelMatrix.rotate(structLaserArm.bodyJoint, 0.0, 1.0, 0.0);  // Rotate around the y-axis
  drawSegment(gl, n, laserArm.g_arm1Buffer, viewProjMatrix, a_Position, texturetype); // Draw

  // Arm2
  var arm2Length = 10.0;
  g_modelMatrix.translate(0.0, arm1Length, 0.0);       // Move to joint1
  g_modelMatrix.rotate(structLaserArm.middleJoint, 1.0, 0.0, 0.0);  // Rotate around the z-axis
  drawSegment(gl, n, laserArm.g_arm2Buffer, viewProjMatrix, a_Position, texturetype); // Draw

  // Arm3
  var arm3Length = 10.0;
  g_modelMatrix.translate(0.0, arm2Length, 0.0);       // Move to joint1
  g_modelMatrix.rotate(structLaserArm.middleJoint, 1.0, 0.0, 0.0);  // Rotate around the z-axis
  drawSegment(gl, n, laserArm.g_arm1Buffer, viewProjMatrix, a_Position, texturetype); // Draw

  // Palm1
  var palm1Length = 2.0;
  g_modelMatrix.translate(0.0, arm3Length, 0.0);       // Move to palm
  g_modelMatrix.rotate(structLaserArm.upperJoint, 0.0, 1.0, 0.0);  // Rotate around the y-axis
  drawSegment(gl, n, laserArm.g_palmBuffer, viewProjMatrix, a_Position, texturetype);  // Draw

  // Move to the center of the tip of the palm1
  g_modelMatrix.translate(0.0, palm1Length, 0.0);

  // Finger1
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0, 0.0, 2.0);
  g_modelMatrix.rotate(structLaserArm.wristJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger1 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(structLaserArm.wristJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger2
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0, 0.0, -2.0);
  g_modelMatrix.rotate(-structLaserArm.wristJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger2 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(-structLaserArm.wristJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger3
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(-2, 0.0, 0);
  g_modelMatrix.rotate(structLaserArm.wristJoint, 0, 0, 1);  // Rotate around the x-axis
  g_modelMatrix.rotate(90, 0, 1, 0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger3 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(-structLaserArm.wristJoint, 1, 0, 0);  // Rotate around the x-axis
  g_modelMatrix.rotate(90, 0, 1, 0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger4
  g_modelMatrix.translate(2, 0.0, 0);
  g_modelMatrix.rotate(-structLaserArm.wristJoint, 0, 0, 1);  // Rotate around the x-axis
  g_modelMatrix.rotate(90, 0, 1, 0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBottomBuffer, viewProjMatrix, a_Position, texturetype);

  // Finger4 top
  g_modelMatrix.translate(0.0, 4.0, 0.0);
  g_modelMatrix.rotate(structLaserArm.wristJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  g_modelMatrix.rotate(90, 0, 1, 0);  // Rotate around the x-axis
  drawSegment(gl, n, laserArm.g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
}

function drawCarrierArm(gl, n, viewProjMatrix, a_Position, texturetype) {
  var baseHeight = 2.0;
  g_modelMatrix.setTranslate(7, -12, -20);
  g_modelMatrix.scale(0.5, 0.5, 0.5);
  drawSegment(gl, n, g_baseBuffer, viewProjMatrix, a_Position, texturetype);

  // Arm1
  var arm1Length = 10.0;
  g_modelMatrix.translate(0.0, baseHeight, 0);     // Move onto the base
  g_modelMatrix.rotate(structCarrierArm.bodyJoint, 0.0, 5.0, 0.0);  // Rotate around the y-axis
  drawSegment(gl, n, g_arm1Buffer, viewProjMatrix, a_Position, texturetype); // Draw

  // Arm2
  var arm2Length = 10.0;
  g_modelMatrix.translate(0.0, arm1Length, 0.0);       // Move to joint1
  g_modelMatrix.rotate(structCarrierArm.middleJoint, 0.0, 0.0, 1.0);  // Rotate around the z-axis
  drawSegment(gl, n, g_arm2Buffer, viewProjMatrix, a_Position, texturetype); // Draw

  // A palm
  var palmLength = 2.0;
  g_modelMatrix.translate(0.0, arm2Length, 0.0);       // Move to palm
  g_modelMatrix.rotate(structCarrierArm.wristJoint, 0.0, 1.0, 0.0);  // Rotate around the y-axis
  drawSegment(gl, n, g_palmBuffer, viewProjMatrix, a_Position, texturetype);  // Draw

  // Move to the center of the tip of the palm
  g_modelMatrix.translate(0.0, palmLength, 0.0);

  // Draw finger1
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(2.0, 0.0, 2.0);
  g_modelMatrix.rotate(structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger2
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(2.0, 0.0, -2.0);
  g_modelMatrix.rotate(-structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger3
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0.0, 0.0, 2.0);
  g_modelMatrix.rotate(structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger4
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(0.0, 0.0, -2.0);
  g_modelMatrix.rotate(-structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger5
  pushMatrix(g_modelMatrix);
  g_modelMatrix.translate(-2.0, 0.0, -2.0);
  g_modelMatrix.rotate(-structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
  g_modelMatrix = popMatrix();

  // Finger6
  g_modelMatrix.translate(-2.0, 0.0, 2.0);
  g_modelMatrix.rotate(structCarrierArm.fingerJoint, 1.0, 0.0, 0.0);  // Rotate around the x-axis
  drawSegment(gl, n, g_fingerBuffer, viewProjMatrix, a_Position, texturetype);
}

main();